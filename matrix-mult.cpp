/*
 * (6) kji implementation
 *
 *  pg27770 - André Oliveira
 *  pg27765 - João Ferreira
 */

#include <iostream>
#include <cstdlib>
#include <cmath>

#include <papi.h>

#include <sys/time.h>


#define TIME_RESOLUTION 1000000	// time measuring resolution (us)

#define UNUSED(X)  ((void)(X))

#define NUM_EVENTS 4
typedef long long unsigned ttime;

unsigned int SIZE;

char * Events[NUM_EVENTS] = {
    "PAPI_L1_DCM",
    "PAPI_L2_TCM",
    "PAPI_L3_TCM",
    "PAPI_TOT_INS"
};

int myEventSet = PAPI_NULL;

long long values [NUM_EVENTS];

ttime start (void)
{
    timeval t;

    gettimeofday(&t, NULL);
    return t.tv_sec * TIME_RESOLUTION + t.tv_usec;
}


ttime stop (ttime initial_time)
{
    timeval t;
    gettimeofday(&t, NULL);
    ttime final_time = t.tv_sec * TIME_RESOLUTION + t.tv_usec;
    return final_time - initial_time;
}

void randomFillMatrix (float **mat) {

    for (unsigned i = 0; i < SIZE; ++i) {
        for (unsigned j = 0; j < SIZE; ++j) {
            //std::cout << i << ' ' << j << '\n';
            mat[i][j] = (float) rand() / (float) RAND_MAX;
        }
    }
}

void numFillMatrix (float **mat, int num) {

    for (unsigned i = 0; i < SIZE; ++i) {
        for (unsigned j = 0; j < SIZE; ++j) {
            //std::cout << i << ' ' << j << '\n';
            mat[i][j] = num;
        }
    }
}

#ifdef TRANSPOSE
void transposeMatrix(float **mat) {
    for (unsigned i = 0; i < SIZE; ++i) {
        for (unsigned j = 0; j < i; ++j) {
            float tmp = mat[i][j];
            mat[i][j] = mat[j][i];
            mat[j][i] = tmp;
        }
    }
}
#endif

void printMat (float **mat)
{
    for (unsigned i = 0; i < SIZE; ++i) {
        for (unsigned j = 0; j < SIZE; ++j) {
            std::cout << mat[i][j] << ' ';
        }
        std::cout << '\n';
    }
    std::cout << '\n';
}

/* Normal */
void matMult (float **matA, float **matB, float **res)
{
    float alpha;
    for (unsigned k = 0; k < SIZE; k++) {
        for (unsigned j = 0; j < SIZE; j++) {
            alpha = matB[k][j];
            for (unsigned i = 0; i < SIZE; i++) {

#ifdef TRANSPOSE
                res[j][i] += matA[k][i] * alpha;
#else
                res[i][j] += matA[i][k] * alpha;
#endif
            }
        }
    }
}

void usage(int argc, char* argv[]) {
    if (argc != 2) {
        std::cout << "Usage: " << argv[0] << ' ' << "<Matrix Size>" << "\n";
        exit(-1);
    }
}

int main(int argc, char *argv[])
{
    usage(argc, argv);

    SIZE = atoi(argv[1]);

    float **matA = new float* [SIZE];
    float **matB = new float* [SIZE];
    float **res  = new float* [SIZE];

    for (unsigned i = 0; i < SIZE; ++i) {
        matA[i] = new float [SIZE];
        matB[i] = new float [SIZE];
        res[i]  = new float [SIZE];
    }

    ttime t, ttotal;
    int retval;
    UNUSED(retval);

    retval = PAPI_library_init(PAPI_VER_CURRENT);
    retval = PAPI_create_eventset(&myEventSet);

    for (int i = 0; i < NUM_EVENTS; i++) {
        retval = PAPI_add_named_event(myEventSet, Events[i]);
        // std::cout << Events[i] << " -> " << retval << '\n';
    }

    randomFillMatrix(matA);
    randomFillMatrix(matB);
    numFillMatrix(res, 0);

#ifdef TRANSPOSE
    std::cout << "-- Transposed Matrix --\n";
    transposeMatrix(matA);
#endif

    t = start();
    retval = PAPI_start(myEventSet);
    matMult(matA, matB, res);
    retval = PAPI_stop(myEventSet, values);
    ttotal = stop(t);

#ifdef TRANSPOSE
    transposeMatrix(res);
#endif

/*
    printMat(matA);
    printMat(matB);
    printMat(res);
*/

    std::cout << "Size : " << SIZE << '\n';
    std::cout << "Execution Time : " << ttotal << '\n';

    for (int i = 0; i < NUM_EVENTS; i++) {
        std::cout << Events[i]  << " : " << values[i] << '\n';
    }
    std::cout << '\n';

    free(matA);
    free(matB);
    free(res);

    return 0;
}
